﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutLoveTimeUI : MonoBehaviour {
    Animator anim;
    public GameObject bea;
    Lovetime ltscript;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        ltscript = bea.GetComponent<Lovetime>();
        anim.StopPlayback();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetAxis(ltscript.inputName) > 0 || Input.GetButtonDown(ltscript.inputName)){
            if (!ltscript.isActivated)
            {
                anim.SetTrigger("UIEnter");
            }
            if (ltscript.isActivated)
            {
                anim.SetTrigger("HitRT");
            }
        }
	}

    //Called in animator
    public void Deleteme()
    {
        Destroy(this);
    }
}
