﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RicochetCollider : MonoBehaviour {

    Ricochet ricochetScript;
    private void Start()
    {
        ricochetScript = GetComponentInParent<Ricochet>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag!="Boost" && other.tag!="Ragdoll")
        ricochetScript.Impact();
    }
}
