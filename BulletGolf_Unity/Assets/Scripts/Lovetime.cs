﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lovetime : MonoBehaviour {

    public BulletMovementController movementController;
    public CameraFollow cameraScript;
    public GameTimer timerScript;
    public RadialMeter meterScript;
    public Bren_Slowmo_Temp slowmoScript;
    public Camera bulletCam;
    public string inputName;
    public float slowFactor;
    public float fastTurnSmoothing;
    public float narrowFOV;
    public float slowRate;
    public float fastRate;
    public float cameraPosSmoothing;
    public bool isActivated = false;

    public float originalForwardSpeed;
    private float originalTurnSmoothing;
    public float originalFOV;
    public float originalCameraPosSmoothing;
    public bool isEnabled;
	// Use this for initialization
	void Start () {
        if(movementController == null)
        {
            Debug.LogError("Movement Controls Script is missing from this object");
        }
        originalForwardSpeed = movementController.forwardSpeed;
        originalTurnSmoothing = movementController.smoothing;
        originalFOV = bulletCam.fieldOfView;
        originalCameraPosSmoothing = cameraScript.smoothingPos;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (isEnabled && meterScript.currentValue > 0)
        {
            //Activate Love Time
            if (Input.GetAxis(inputName) > 0 || Input.GetButtonDown(inputName) && !isActivated)
            {
                isActivated = true;
                movementController.inLoveTime = true;
                meterScript.MeterEnabled(true);
                slowmoScript.slowdown = true;
            }
            //Deactivate Love Time
            if (Input.GetAxis(inputName) < 1 || Input.GetButtonUp(inputName) && isActivated)
            {
                isActivated = false;
                movementController.inLoveTime = false;
                meterScript.MeterEnabled(false);
                slowmoScript.slowdown = false;

            }
        }
        else
        {
            isActivated = false;
            movementController.inLoveTime = false;
            meterScript.MeterEnabled(false);
            slowmoScript.slowdown = false;

        }

        if (isActivated)
        {
            //subtract slow factor to determine speed to change to
            float loveTimeSpeed = originalForwardSpeed - slowFactor;
            //Slow down based on rate given
            if(movementController.forwardSpeed != loveTimeSpeed)
            {
                movementController.forwardSpeed = Mathf.Lerp(movementController.forwardSpeed, loveTimeSpeed, Time.deltaTime * slowRate);
            }
            //Increase FOV based on rate given
            if(bulletCam.fieldOfView != narrowFOV)
            {
                bulletCam.fieldOfView = Mathf.Lerp(bulletCam.fieldOfView, narrowFOV, Time.deltaTime * slowRate);

            }
            //Immediately change turn rate
            if (movementController.smoothing != fastTurnSmoothing)
            {
                movementController.smoothing = fastTurnSmoothing;
            }

            cameraScript.smoothingPos = cameraPosSmoothing;
        } else if (!isActivated)
        {
            //Speed up to original speed based on rate given
            if (movementController.forwardSpeed < originalForwardSpeed)
            {
                movementController.forwardSpeed = Mathf.Lerp(movementController.forwardSpeed, originalForwardSpeed, Time.deltaTime * fastRate);
            }
            //Return to regular FOV based on rate given
            if (bulletCam.fieldOfView != originalFOV)
            {
                bulletCam.fieldOfView = Mathf.Lerp(bulletCam.fieldOfView, originalFOV, Time.deltaTime * slowRate);

            }
            //Immediately change turn rate
            if (movementController.smoothing != originalTurnSmoothing)
            {
                movementController.smoothing = originalTurnSmoothing;
            }
            cameraScript.smoothingPos = originalCameraPosSmoothing;
        }
        timerScript.timeMultiplier = (movementController.forwardSpeed / originalForwardSpeed);

    }
}
