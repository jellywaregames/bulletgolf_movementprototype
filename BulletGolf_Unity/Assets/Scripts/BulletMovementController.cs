﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovementController : MonoBehaviour
{
    [Header("Input Names")]
    public string bulletHorizontal;
    public string bulletVertical;
    [Header("Forward Speed")]
    public float forwardSpeed;
    public float tempSpeed;
    [Header("Turn Multipliers")]
    public float turnIncreaseIncrement;
    public float multiplierVertical;
    public float multiplierHorizontal;
    public float smoothing;
    [Header("Vertical Constrain")]
    public bool canConstrainVertical;
    public float constraintAngleVertical;
    [Header("Horizontal Constrain")]
    public bool canConstrainHorizontal;
    public float constraintAngleHorizontal;

    private float bulletAxisX, bulletAxisY;
    private Rigidbody m_rigidbody;
    private Vector3 targetRotation;
    public bool useGradualTurn;

    public static bool invertY;
    public bool inLoveTime;
    public bool isEnabled;
    // Use this for initialization
    void Start()
    {
        m_rigidbody = this.GetComponent<Rigidbody>();
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("InvertY"))
        {
            invertY = !invertY;
        }
        if (isEnabled)
        {
            if (m_rigidbody.isKinematic == true)
                m_rigidbody.isKinematic = false;

            if (useGradualTurn)
            {
                //Increaseing bulletAxisX gradually
                float horizontalInput = Input.GetAxisRaw(bulletHorizontal), verticalInput = Input.GetAxisRaw(bulletVertical);
                float tempIncrementX = 0, tempIncrementY = 0;

                // determining whether increment should be positive or negative
                {
                    if (horizontalInput > 0)
                        tempIncrementX = turnIncreaseIncrement;
                    else if (horizontalInput < 0)
                        tempIncrementX = turnIncreaseIncrement * -1;
                    else
                    {
                        if (bulletAxisX > 0)
                            tempIncrementX = turnIncreaseIncrement * -1;
                        else if (bulletAxisX < 0)
                            tempIncrementX = turnIncreaseIncrement;
                    }

                    if (verticalInput > 0)
                        tempIncrementY = turnIncreaseIncrement;
                    else if (verticalInput < 0)
                        tempIncrementY = turnIncreaseIncrement * -1;
                    else
                    {
                        if (bulletAxisY > 0)
                            tempIncrementY = turnIncreaseIncrement * -1;
                        else if (bulletAxisY < 0)
                            tempIncrementY = turnIncreaseIncrement;
                    }
                }
                // calculating bulletAxi by increments
                {
                    if (Mathf.Abs(horizontalInput) > 0)
                    {
                        bulletAxisX += tempIncrementX;
                        if (Mathf.Abs(bulletAxisX) > Mathf.Abs(horizontalInput))
                        {
                            bulletAxisX = horizontalInput;
                        }
                    }
                    else
                    {
                        bulletAxisX += tempIncrementX;
                        if (Mathf.Abs(bulletAxisX) < .1f)
                        {
                            bulletAxisX = 0;
                        }
                    }
                    //Increaseing bulletAxisY gradually
                    if (Mathf.Abs(verticalInput) > 0)
                    {
                        bulletAxisY += tempIncrementY;
                        if (Mathf.Abs(bulletAxisY) > Mathf.Abs(verticalInput))
                        {
                            bulletAxisY = verticalInput;
                        }
                    }
                    else
                    {
                        bulletAxisY += tempIncrementY;
                        if (Mathf.Abs(bulletAxisY) < .1f)
                        {
                            bulletAxisY = 0;
                        }
                    }
                }
            }
            else
            {
                //Gets input from gamepad/arrow keys
                bulletAxisX = Input.GetAxisRaw(bulletHorizontal);
                bulletAxisY = Input.GetAxisRaw(bulletVertical);
            }
        }
        else
        {
            if (bulletAxisX != 0)
                bulletAxisX = 0;
            if (bulletAxisY != 0)
                bulletAxisY = 0;
            if (m_rigidbody.isKinematic == false)
                m_rigidbody.isKinematic = true;
        }
        float inverseFactor = -1;

        if (invertY)
        {
            inverseFactor = 1;
        }

        //Get the target rotation by multipling the axis by the multiplier value
        targetRotation = this.transform.eulerAngles + new Vector3(bulletAxisY * (multiplierVertical * inverseFactor), bulletAxisX * multiplierHorizontal, 0);

        if (canConstrainVertical)
        {
            //Save x value
            float targetRotationVertical = targetRotation.x;

            //Constrain the x value within the given angles
            if (targetRotationVertical > 180 && targetRotationVertical < 360 - constraintAngleVertical)
            {
                targetRotationVertical = 360 - constraintAngleVertical;
            }
            else if (targetRotationVertical < 180 && targetRotationVertical > constraintAngleVertical)
            {
                targetRotationVertical = constraintAngleVertical;
            }
            targetRotation.x = targetRotationVertical;

        }

        if (canConstrainHorizontal)
        {
            //Save y value
            float targetRotationHorizontal = targetRotation.y;

            //Constrain the y value within the given angles
            if (targetRotationHorizontal > 180 && targetRotationHorizontal < 360 - constraintAngleHorizontal)
            {
                targetRotationHorizontal = 360 - constraintAngleHorizontal;
            }
            else if (targetRotationHorizontal < 180 && targetRotationHorizontal > constraintAngleHorizontal)
            {
                targetRotationHorizontal = constraintAngleHorizontal;
            }
            targetRotation.y = targetRotationHorizontal;

        }
        if (isEnabled)
        {
            this.transform.eulerAngles = Vector3.Lerp(this.transform.eulerAngles, targetRotation, smoothing * Time.deltaTime);
        }

    }

    private void FixedUpdate()
    {
        float combinedSpeed = 0;
        if (!inLoveTime)
            combinedSpeed = forwardSpeed + tempSpeed;
        else if (inLoveTime)
            combinedSpeed = forwardSpeed;

        if (isEnabled)
        {
            m_rigidbody.velocity = transform.forward * combinedSpeed;
        }
    }
}
