﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : MonoBehaviour {
    public float maxSpeed;
    public float speedIncrement;
    public float FOVIncrement;

    public bool affectSlowFactor;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Lovetime loveTimeScript = other.GetComponentInParent<Lovetime>();
            if (loveTimeScript)
            {
               if(loveTimeScript.originalForwardSpeed < maxSpeed)
                {
                    loveTimeScript.originalForwardSpeed += speedIncrement;
                    if (affectSlowFactor)
                    {
                        loveTimeScript.slowFactor += speedIncrement;
                    }
                    if(loveTimeScript.originalForwardSpeed > maxSpeed)
                    {
                        loveTimeScript.originalForwardSpeed = maxSpeed;
                    }
                    loveTimeScript.originalFOV += FOVIncrement;
                //    Destroy(this.gameObject);
                } else if (loveTimeScript.originalForwardSpeed == maxSpeed)
                {
                    return;
                }
            }

        }
        
    }
}
