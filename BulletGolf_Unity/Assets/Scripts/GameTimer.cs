﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {
    public Text text1, text2;
    public float time;
    public float timeMultiplier;
    public bool timeOn;

    // Use this for initialization
    void Start () {
        //resultsScript = GameObject.Find("Results_Canvas").gameObject.GetComponent<Results_Screen>();
        text1.text = "0.00";
        text2.text = "0.00";

    }

    // Update is called once per frame
    void Update () {
		if (timeOn)
        {
            time += Time.deltaTime * timeMultiplier;
           // float roundedTime = (Mathf.Round(time * 100) / 100);
            string timeString = time.ToString("0.00");
            text1.text = timeString;
            text2.text = timeString;
        }

    }

    public void ToggleTimer()
    {
        timeOn = !timeOn;
    }
    public void TimerOn()
    {
        timeOn = true;
    }
    public void TimerOff()
    {

        timeOn = false;
    }
}
