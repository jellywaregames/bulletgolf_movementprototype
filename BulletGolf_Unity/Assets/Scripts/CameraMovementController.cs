﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementController : MonoBehaviour {

    [Header("Input Names")]
    public string cameraHorizontal;
    public string cameraVertical;
    [Header("Turn Multipliers")]
    public float multiplierVertical;
    public float multiplierHorizontal;
    public float smoothing;
    [Header("Vertical Constrain")]
    public bool canConstrainVertical;
    public float constraintAngleVertical;
    [Header("Horizontal Constrain")]
    public bool canConstrainHorizontal;
    public float constraintAngleHorizontal;

    private float cameraAxisX, cameraAxisY;
    private Vector3 targetRotation;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //Gets input from gamepad/arrow keys
        cameraAxisX = Input.GetAxisRaw(cameraHorizontal);
        cameraAxisY = Input.GetAxisRaw(cameraVertical);

        //Get the target rotation by multipling the axis by the multiplier value
        targetRotation = this.transform.eulerAngles + new Vector3(cameraAxisY * multiplierVertical, cameraAxisX * multiplierHorizontal, 0);

        if (canConstrainVertical)
        {
            //Save x value
            float targetRotationVertical = targetRotation.x;

            //Constrain the x value within the given angles
            if (targetRotationVertical > 180 && targetRotationVertical < 360 - constraintAngleVertical)
            {
                targetRotationVertical = 360 - constraintAngleVertical;
            }
            else if (targetRotationVertical < 180 && targetRotationVertical > constraintAngleVertical)
            {
                targetRotationVertical = constraintAngleVertical;
            }
            targetRotation.x = targetRotationVertical;

        }

        if (canConstrainHorizontal)
        {
            //Save y value
            float targetRotationHorizontal = targetRotation.y;

            //Constrain the y value within the given angles
            if (targetRotationHorizontal > 180 && targetRotationHorizontal < 360 - constraintAngleHorizontal)
            {
                targetRotationHorizontal = 360 - constraintAngleHorizontal;
            }
            else if (targetRotationHorizontal < 180 && targetRotationHorizontal > constraintAngleHorizontal)
            {
                targetRotationHorizontal = constraintAngleHorizontal;
            }
            targetRotation.y = targetRotationHorizontal;

        }
        this.transform.eulerAngles = Vector3.Lerp(this.transform.eulerAngles, targetRotation, smoothing * Time.deltaTime);


    }



}
