﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFollow : MonoBehaviour {

    [Header("Forward Speed")]
    public float forwardSpeed;

    public Transform target;
    public float smoothingRot;

    private Rigidbody m_rigidbody;

    // Use this for initialization
    void Start () {
        m_rigidbody = this.GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update () {
        if (target.transform.forward != transform.forward)
        {
            Vector3 targetDir = Vector3.RotateTowards(transform.forward, target.transform.forward, smoothingRot * Time.deltaTime, 0);
            transform.rotation = Quaternion.LookRotation(targetDir);

        }


    }

    private void FixedUpdate()
    {
        m_rigidbody.velocity = transform.forward * forwardSpeed;

    }


}
