﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class Bren_Slowmo_Temp : MonoBehaviour {

    public bool slowdown;
    public BulletMovementController moveScript;
    float initialSpeed;
    public float slowSpeed;
    public PostProcessingProfile postProc;
    public float vignetteIntensity;
    VignetteModel.Settings vignetteSettings;
    ParticleSystem partSys;
    TrailRenderer trailRen;
    float initialTrailWidth;
    public bool isEnabled;

    void Start () {
        moveScript = this.GetComponent<BulletMovementController>();
        partSys = this.GetComponent<ParticleSystem>();
        trailRen = this.GetComponent<TrailRenderer>();
        initialTrailWidth = trailRen.startWidth;

        initialSpeed = moveScript.forwardSpeed;
        vignetteSettings = postProc.vignette.settings;

    }


    void Update () {
        CheckSlowDown();
      //  AdjustBulletSpeed();
        AdjustVignette();
        TogglePartSysAndTrailRenderer();
    }

    void CheckSlowDown()
    {/*
        if (isEnabled)
        {

            if (Input.GetButton("LoveTime") || Input.GetAxis("LoveTime") > 0)
            {
                slowdown = true;
            }
            else
            {
                slowdown = false;
            }
        } else if (!isEnabled)
        {
            slowdown = false;
        }
        */
    }

    void AdjustBulletSpeed()
    {
        if (slowdown)
        {
            moveScript.forwardSpeed = slowSpeed;

        }
        else
        {
            moveScript.forwardSpeed = initialSpeed;
        }
    }

    void AdjustVignette()
    {
        if (slowdown)
        {
            vignetteSettings.color = Color.black;
            vignetteSettings.intensity = Mathf.MoveTowards(vignetteSettings.intensity, vignetteIntensity, 1.5f * Time.deltaTime);
        } else
        {
            vignetteSettings.color = Color.black;

            vignetteSettings.intensity = Mathf.MoveTowards(vignetteSettings.intensity, 0f, 1.5f * Time.deltaTime);
        }
        postProc.vignette.settings = vignetteSettings;
    }

    void TogglePartSysAndTrailRenderer()
    {
        if (slowdown)
        {
            var em = partSys.emission;
            em.rateOverTime = 25;

            trailRen.startWidth = Mathf.MoveTowards(trailRen.startWidth, 0f, Time.deltaTime);
        }

        else
        {
            var em = partSys.emission;
            em.rateOverTime = 0;

            trailRen.startWidth = Mathf.MoveTowards(trailRen.startWidth, initialTrailWidth, Time.deltaTime);

        }
    }
}
