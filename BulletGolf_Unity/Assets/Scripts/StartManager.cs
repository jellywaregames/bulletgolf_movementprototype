﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartManager : MonoBehaviour
{
    public string inputName;
    public bool isReady;
    [Header("Scripts to Enable")]
    public BulletMovementController movementController;
    public Lovetime loveTimeScript;
    public Bren_Slowmo_Temp slowmoScript;
    public GameTimer timerScript;

    // Use this for initialization
    void Start()
    {
        isReady = true;
        movementController.isEnabled = false;
        loveTimeScript.isEnabled = false;
        slowmoScript.isEnabled = false;
        timerScript.TimerOff();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis(inputName) > 0 || Input.GetButtonDown(inputName))
        {
            if (isReady)
            {
                timerScript.TimerOn();
                movementController.isEnabled = true;
                isReady = false;
            }

        }
        if (Input.GetAxis(inputName) < 1 || Input.GetButtonUp(inputName))
        {
            if (!isReady)
            {
                loveTimeScript.isEnabled = true;
                slowmoScript.isEnabled = true;
            }

        }

    }
}
