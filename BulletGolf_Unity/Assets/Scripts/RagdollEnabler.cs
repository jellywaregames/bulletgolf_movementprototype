﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollEnabler : MonoBehaviour {
    public bool test = false;
	// Use this for initialization
	void Awake () {
		
	}

    private void Update()
    {
        if (test)
        {
            EnableRagdoll();
        }
    }

    public void EnableRagdoll()
    {
        Component[] rb;

        rb = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody r in rb)
        {
            r.isKinematic = false;
        }
    }

}
