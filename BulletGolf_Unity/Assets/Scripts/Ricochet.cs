﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ricochet : MonoBehaviour
{

    BulletMovementController bullet;
    public CameraFollow cameraFollow;
    public GameObject sparkPrefab;
    GameObject[] sparkSystems;
    [Tooltip("Max distance for detecting ricochet")]
    public float maxDistance = 10;
    [Tooltip("length of slow down effect")]
    public float duration = 3;
    [Tooltip("Speed Reduction Curve")]
    public AnimationCurve curve, cameraCurve;
    [Tooltip("transform at the tip of the bullet")]
    public Transform bulletFront;
    Vector3 startVector, impactPoint, reflectedVector;
    bool approachingRicochet, isRicochetting;
    LineRenderer line;
    private float bulletSpeed;

    [HideInInspector]
    public bool isImpacting;

    private void Start()
    {
        sparkSystems = new GameObject[20];
        int sparkIndex = 0;
        for (int i = 0; i < sparkSystems.Length; i++)
        {
            GameObject newSpark = (GameObject)Instantiate(sparkPrefab, transform.position, Quaternion.identity);
            sparkSystems[i] = newSpark;
            sparkSystems[i].SetActive(false);
            sparkIndex++;
        }
        if (sparkPrefab == null)
            Debug.LogError("bullet needs spark prefab");
        else
            sparkPrefab.SetActive(false);
        bullet = GetComponent<BulletMovementController>();
        line = GetComponent<LineRenderer>();
        cameraFollow = GameObject.Find("Camera Rotation").GetComponent<CameraFollow>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!isImpacting)
        {
            PredictRicochet();

            if (approachingRicochet)
                line.enabled = true;
            else
                line.enabled = false;
        }
        else
        {
            if (!isRicochetting)
                AnimateRicochet();
        }
    }
    public void Impact()
    {

        if (isRicochetting)
        {
            StopAllCoroutines();
            isRicochetting = false;

            cameraFollow.ricochetSlow = 1;
            bullet.forwardSpeed = bulletSpeed;
            PredictRicochet();
        }
        else
            isImpacting = true;
    }

    void PredictRicochet()
    {
        approachingRicochet = false;
        RaycastHit hit;
        if (Physics.Raycast(bulletFront.position, bullet.transform.forward * .1f, out hit, maxDistance))
        {
            if (hit.transform.tag != "Boost" && hit.transform.tag != "Ragdoll")
            {
                startVector = bullet.transform.forward;
                impactPoint = hit.point;
                reflectedVector = Vector3.Reflect(startVector, hit.normal);

                //Debug.DrawRay(hit.point, reflectedVector,Color.cyan);
                line.SetPosition(0, bullet.transform.position);
                line.SetPosition(1, hit.point);
                line.SetPosition(2, hit.point + (reflectedVector * 20));

                approachingRicochet = true;
                if ((impactPoint - bullet.transform.position).magnitude < 1)
                    Impact();
            }
        }
    }

    void AnimateRicochet()
    {
        //untether camera, lock control
        //cameraFollow.lerpCamera = false;

        //set bullet on new trajectory but slow to a stop
        bullet.transform.position = impactPoint + reflectedVector;
        bullet.transform.rotation = Quaternion.LookRotation(reflectedVector, Vector3.up);
        StartCoroutine(RicochetEffect());

        isRicochetting = true;
    }

    IEnumerator RicochetEffect()
    {
        float current = 0;
        bulletSpeed = bullet.forwardSpeed;

        foreach (GameObject spark in sparkSystems)
        {
            if (!spark.activeSelf)
            {
                spark.transform.position = impactPoint;
                //change i just made to test prediction methods
                isImpacting = false;
                spark.SetActive(true);
                break;
            }
        }
        while (current < duration)
        {
            current += Time.deltaTime;
            bullet.forwardSpeed = bulletSpeed * curve.Evaluate(current / duration);

            cameraFollow.ricochetSlow = cameraCurve.Evaluate(current / duration);

            //re-tether camera
            //if (current > duration / 2)
                //  cameraFollow.lerpCamera = true;
                yield return 0f;
        }
        cameraFollow.ricochetSlow = 1;
        bullet.forwardSpeed = bulletSpeed;


        //continue speed, return control
        isImpacting = false;
        isRicochetting = false;
        foreach (GameObject spark in sparkSystems)
        {
            spark.SetActive(false);
        }

    }
}
