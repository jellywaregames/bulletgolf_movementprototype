﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableObject_Coffee : MonoBehaviour {

    public GameObject bullet;
    public GameObject child;
    public GameObject cameraParticles;
    Transform particles;
    GameObject[] particleList;

    private Collider m_collider;

	void Start () {
        particles = gameObject.transform.Find("particles").gameObject.transform;
        particleList = new GameObject[particles.transform.childCount];

        for (int i = 0; i < particles.transform.childCount; i++)
        {
            particleList[i] = particles.GetChild(i).gameObject;
        }

        m_collider = this.GetComponent<Collider>();
	}

    private void Update()
    {
        Debug.DrawLine(transform.position, transform.position + (Vector3.up.normalized * 5), Color.yellow);
    }

    public void OnTriggerEnter(Collider collision) {
    
            Debug.Log("Hit");
        if (collision.gameObject.tag == "Player")
        {
            print("BOOM");
            this.GetComponent<Renderer>().enabled = false;
            child.GetComponent<Renderer>().enabled = false;

            transform.rotation = bullet.transform.rotation;
            //transform.LookAt = (transform.position + bullet.transform.forward);
            //particles.RotateAround(transform.position, Vector3.up, bullet.transform.rotation.y);
        
            PlayParticles();
            m_collider.enabled = false;
        }
    }

    public void PlayParticles()
    {
        for (int i = 0; i < particleList.Length; i++)
        {
            particleList[i].GetComponent<ParticleSystem>().Play();
            cameraParticles.GetComponent<ParticleSystem>().Play();
        }
    }
}

