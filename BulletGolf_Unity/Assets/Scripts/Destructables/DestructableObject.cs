﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableObject : MonoBehaviour {

    Transform particles;
    GameObject[] particleList;

	void Start () {
        particles = gameObject.transform.Find("particles").gameObject.transform;
        particleList = new GameObject[particles.transform.childCount];

        for (int i = 0; i < particles.transform.childCount; i++)
        {
            particleList[i] = particles.GetChild(i).gameObject;
        }
	}

    public void OnTriggerEnter(Collider collision) {
    
            Debug.Log("Hit");
        if (collision.gameObject.tag == "Bullet")
        {
            print("BOOM");
            this.GetComponent<Renderer>().enabled = false;
            PlayParticles();
        }
    }

    public void PlayParticles()
    {
        for (int i = 0; i < particleList.Length; i++)
        {
            particleList[i].GetComponent<ParticleSystem>().Play();
        }
    }
}

