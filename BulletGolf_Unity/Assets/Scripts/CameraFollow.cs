﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform followTarget, rotationTarget;
    public float smoothingPos;
    public float smoothingRot;
    public float maxDistance;
    public Vector3 offset;
    private Vector3 targetCamPos;

    public float ricochetSlow = 1;
    // Use this for initialization
    void Start()
    {
        offset = this.transform.position - followTarget.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (followTarget != null)
        {
            //Find the target position for the camera
            targetCamPos = followTarget.position + offset;
        }
        else if (followTarget == null)
        {
            Debug.LogError("No target given to camera");
        }



        float targetDistance = Vector3.Distance(targetCamPos, this.transform.position);

        if (targetDistance > maxDistance)
        {
            float difference = targetDistance - maxDistance;
            Vector3 targetDir = (followTarget.position - this.transform.position).normalized * difference;
            //       Vector3 newPos = targetDir * 3;
            this.transform.position = this.transform.position + targetDir;
        }

        transform.position = Vector3.Lerp(this.transform.position, targetCamPos, ricochetSlow * smoothingPos * Time.deltaTime);





            if (rotationTarget.transform.forward != transform.forward)
            {
                Vector3 targetDir = Vector3.RotateTowards(transform.forward, rotationTarget.transform.forward, ricochetSlow * smoothingRot * Time.deltaTime, 0);
                transform.rotation = Quaternion.LookRotation(targetDir);

            }
 


     

    }

    private void LateUpdate()
    {

        //Lerp the camera to the given position
        //Jesse Has added this conditional to control camera lerping



    }
}
