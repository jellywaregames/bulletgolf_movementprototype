﻿using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class Curve : MonoBehaviour {

    public Vector3 [] points;
    public List<GameObject> objects = new List<GameObject>();
    public int numberOfSegments = 10;
    public GameObject prefab;
    public void Reset()
    {
        points = new Vector3[]
        {
            new Vector3(1f, 0f, 0f),
            new Vector3(2f, 0f, 0f),
            new Vector3(3f, 0f, 0f)
        };
    }
    public Vector3 GetPoint(float t)
    {
        return transform.TransformPoint(Bezier.GetPoint(points[0], points[1], points[2], t));
    }

    private void OnDestroy()
    {
        if (objects.Count > 0)
        {
            foreach (GameObject thing in objects)
            {
                DestroyImmediate(thing);
            }
            objects.Clear();
        }
    }
    // determine if objects are already created, if so destory them and create new ones
    public void setPositions(Vector3[] lineSegments)
    {
        if (objects.Count > 0)
        {
            foreach(GameObject thing in objects)
            {
                DestroyImmediate(thing);
            }
            objects.Clear();
        }
        CreateObjects(lineSegments);
    }
    void CreateObjects(Vector3[] lineSegments)
    {
        foreach (Vector3 position in lineSegments)
        {
            GameObject newObject = (GameObject)Instantiate(prefab, position, Quaternion.identity);
            objects.Add(newObject);
        }
    }
}
