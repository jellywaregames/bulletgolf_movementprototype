﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporarySpeedBoost : MonoBehaviour
{
    public TrailRenderer m_trailRenderer;
    public BulletMovementController movementController;
    public Lovetime loveTimeScript;
    public float speedIncrement;
    public float decreaseRate;

    Color originalColor;
    // Use this for initialization
    void Start()
    {
        //retrieve components if not set
        if (movementController == null)
            movementController = this.GetComponent<BulletMovementController>();
        if (loveTimeScript == null)
            loveTimeScript = this.GetComponent<Lovetime>();
        m_trailRenderer = this.GetComponent<TrailRenderer>();
        originalColor = m_trailRenderer.startColor;
    }

    // Update is called once per frame
    void Update()
    {
        //if temp speed exists, decrease according to the decrease rate
        if (movementController.tempSpeed > 0.01f)
        {
            movementController.tempSpeed = Mathf.Lerp(movementController.tempSpeed, 0, Time.deltaTime * decreaseRate);
            if (loveTimeScript.isActivated == false)
            {
                m_trailRenderer.startColor = Color.Lerp(m_trailRenderer.startColor, originalColor, Time.deltaTime );
            }
        }
        //resets temp speed to 0 after it gets minutely low (otherwise continues to decrease towards 0)
        else if (movementController.tempSpeed < 0.01f)
        {
            movementController.tempSpeed = 0;
            if (loveTimeScript.isActivated == false)
            {
                m_trailRenderer.startColor = originalColor;

            }
        }



    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Temporary")
        {
            movementController.tempSpeed += speedIncrement;
            m_trailRenderer.startColor = Color.white;
        }
    }

}
