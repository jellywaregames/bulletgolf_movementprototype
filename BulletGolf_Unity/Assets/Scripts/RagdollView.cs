﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollView : MonoBehaviour {

    public Transform target;
    public bool isEnabled;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //When enabled rotate to follow target position
        if (isEnabled)
        {
            transform.LookAt(target);
        }
		
	}
}
