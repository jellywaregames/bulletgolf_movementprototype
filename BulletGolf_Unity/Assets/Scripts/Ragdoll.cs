﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    public BulletMovementController movementController;
    public Lovetime loveTimeScript;
    public Bren_Slowmo_Temp slowmoScript;
    public ResetLevel resetLevel;
    public RagdollView cameraScript;
    public GameTimer timerScript;
    public GameTimer resultsScript;
    public GameObject ragdoll, playermodel;
    bool isRagdoll = false;
    public Rigidbody ragdollRigidbody;
    public Rigidbody m_Rigidbody;
    public float resetDelayCrash, resetDelayHit;
    void Awake()
    {
        ragdoll.SetActive(false);
        playermodel.SetActive(true);
    }

    void Start()
    {

    }

    void Update()
    {
        //Manually ragdoll
        if (Input.GetButtonDown("Ragdoll"))
        {

            SetRagdoll(resetDelayCrash);
        }

    }

    //When called, enable ragdoll function
    public void SetRagdoll(float resetDelay)
    {
        if (!isRagdoll)
        {
            //Swap to ragdoll body and apply a force to it
            ragdoll.transform.position = playermodel.transform.position;
            ragdoll.transform.rotation = playermodel.transform.rotation;
            playermodel.SetActive(false);
            ragdoll.SetActive(true);
            ragdollRigidbody.AddForce(m_Rigidbody.velocity * (10), ForceMode.Impulse);


        }
        //Disable player controls
        movementController.isEnabled = false;
        loveTimeScript.isEnabled = false;
        slowmoScript.isEnabled = false;
        //Set camera to follow ragdoll body
        cameraScript.isEnabled = true;
        timerScript.TimerOff();
        //Invoke resetting the level
        resetLevel.Invoke("Reset", resetDelay);
    }

    //On collision, ragdoll
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Target")
        {
            SetRagdoll(resetDelayHit);
        } else {
            SetRagdoll(resetDelayCrash);
        }


    }
}
