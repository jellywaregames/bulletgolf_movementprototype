﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;

public class ScriptedCameraController : MonoBehaviour
{
    private static ScriptedCameraController instance;
    private static string currentScene;
    bool flythroughHasPlayed;
    BulletMovementController beaController;
    CinemachineVirtualCamera flythroughCamera;

    // Use this for initialization
    private void Awake()
    {
        //don't destroy on load
        DontDestroyOnLoad(this);

        //check for instance of this script, if none make this instance
        //if there is an instance check if currentScene variable matches the active scene
        //if not destory instance object, if it does destory this object.
        if (instance == null)
        {
            instance = this;
            Debug.Log("instance is null");
        }
        else if (SceneManager.GetActiveScene().name == currentScene)
        {
            Destroy(this.gameObject);
            Debug.Log("current scene has not changed");
        }
        else
        {
            Destroy(instance.gameObject);
            instance = this;
            Debug.Log("current scene has changed");
        }
        currentScene = SceneManager.GetActiveScene().name;
        Debug.Log(currentScene);

        //disable bea controller until flythrough played
        //beaController = GameObject.Find("Bea").GetComponent<BulletMovementController>();
        //beaController.isEnabled = false;

        //aquire references to flythrough cam and player cam
        //if playing for first time enable flythrough and automatically play through 
        //if same scene enable player cam immediately and no animation
        flythroughCamera = GetComponentInChildren<CinemachineVirtualCamera>();
    }


    private void OnDisable()
    {

        //enable bea controller
        //beaController.isEnabled = true;
    }
}
