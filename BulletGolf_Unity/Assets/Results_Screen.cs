﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Results_Screen : MonoBehaviour
{

    public GameObject[] awards;
    public GameObject slowAward;
    public Text[] targetTimes;

    [SerializeField]
    private GameTimer timerScript;
    public Text best;
    public Text your;

    [Space]
    [Space]
    public float[] timeTargets;
    public float slowestTime;

    [Space]
    public float screenDelay = 1;
    public float bestTime;
    public float lastTime;

    public GameTimer TimerScript
    {
        get
        {
            if (timerScript == null)
                timerScript = FindObjectOfType<GameTimer>(); ;

            return timerScript;
        }
    }


    void Start()
    {
        float[] timeTargets = new float[3];
        GameObject[] awards = new GameObject[5];
        Text[] targetTimes = new Text[3];
        HideScreen(true);
    }

    void UpdateScreen()
    {
        CheckTimeAwards();

        best.text = "Best: " + bestTime;
        your.text = (Mathf.Round(lastTime * 100) / 100).ToString();
    }
    void HideScreen(bool hide)
    {
        if (hide)
        {
            GetComponent<Canvas>().enabled = false;
        }
        else if (!hide)
        {
            GetComponent<Canvas>().enabled = true;
        }
    }

    public IEnumerator ShowDelay(float delay)
    {
        if (delay == null)
        {
            delay = screenDelay;
        }
        yield return new WaitForSeconds(delay);

        HideScreen(false);

        TimerScript.text1.enabled = false;
        TimerScript.text2.enabled = false;

        UpdateScreen();
    }


    void CheckTimeAwards()
    {
        lastTime = TimerScript.time;

        if (lastTime <= timeTargets[0] && lastTime > timeTargets[1])
        { // mediocre time
            awards[0].SetActive(true);
            Debug.Log("MORTY YOU HAVE TO LICK MY BALLS MORTY");
        }
        if (lastTime <= timeTargets[1] && lastTime > timeTargets[2]) //moderate time
        {
            awards[0].SetActive(true);
            awards[0].transform.GetChild(0).gameObject.SetActive(false);

            awards[1].SetActive(true);
            Debug.Log("nice time");
        }
        if (lastTime <= timeTargets[2])
        { //best time

            awards[0].SetActive(true);
            awards[0].transform.GetChild(0).gameObject.SetActive(false);

            awards[1].SetActive(true);
            awards[1].transform.GetChild(0).gameObject.SetActive(false);

            awards[2].SetActive(true);
            Debug.Log("very very nice time");
        }

        if (lastTime >= 20f)
        {
            slowAward.SetActive(true);
        }
    }

    void Math()
    {
        /*
     for (int i = 0; i < timeTargets.Length; i++)
     {
         if ((timeTargets[i] % (Mathf.Round(timeTargets[i])) == 0))
         {
             targetTimes[i].text = timeTargets[i] + ":00";

         }else if ((timeTargets[i] % ((Mathf.RoundToInt(timeTargets[i]) * 10f) / 10f) == 0)){

         targetTimes[i].text = timeTargets[i] + ":" + (timeTargets[i] - (Mathf.Round(timeTargets[i])) + "0");
         }
         else if ((timeTargets[i] % (Mathf.RoundToInt(timeTargets[i]) * 100f) / 100f) == 0)
         {

         targetTimes[i].text = timeTargets[i] + ":" + (timeTargets[i] - (Mathf.Round(timeTargets[i])));

         }
     }
     */
    }
}
