﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaTargetCollision : MonoBehaviour
{
    public const string TAG = "<color=red><b>Daylan</b></color>";
    //public const string MSG_INTATIATE = "Had to instantiate a ";
    //public const string TAG_MSG_ISNT = TAG + " " + MSG_INTATIATE;

    public float forceStrength;
    public GameObject Timer;

    [SerializeField]
    [HideInInspector]
    protected Results_Screen resultsScript;
    [SerializeField]
    [HideInInspector]
    protected Canvas targetCanvas;

    public Results_Screen ResultsScript
    {
        get
        {
            if (resultsScript == null)
                resultsScript = FindObjectOfType<Results_Screen>();

            if (resultsScript == null)
            {
                resultsScript = (Instantiate(Resources.Load<GameObject>("Results_Canvas"), null)).GetComponentInChildren<Results_Screen>();
                //Debug.LogWarning("Intantiated a ResultsCanvas because none are in the scene! Add one to the scene!!");
                Debug.LogWarningFormat("{2}: Intantiated a {0} because none are in the scene! Add one to the scene!! Obj: {1}", resultsScript.GetType().Name, resultsScript.name, TAG);
            }
            return resultsScript;
        }
    }
    public Canvas TargetCanvas
    {
        get
        {
            if (targetCanvas == null)
                targetCanvas = GameObject.Find("TargetIndicator_Canvas").GetComponent<Canvas>();

            return targetCanvas;
        }
    }


    // Update is called once per frame
    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Target")
        {

            Rigidbody targetrb = collision.gameObject.GetComponent<Rigidbody>();
            collision.transform.root.gameObject.SendMessage("EnableRagdoll");
            targetrb.AddForce(this.gameObject.transform.forward * forceStrength);

            Timer.SendMessage("TimerOff");


            ResultsScript.StartCoroutine("ShowDelay", 1f);
            TargetCanvas.enabled = false;
        }
    }

#if UNITY_EDITOR
    private void Reset()
    {
        if (resultsScript == null)
            resultsScript = FindObjectOfType<Results_Screen>();

        if (targetCanvas == null)
            targetCanvas = GameObject.Find("TargetIndicator_Canvas").GetComponent<Canvas>();
    }
    private void OnValidate()
    {
        Reset();
    }
#endif
}
