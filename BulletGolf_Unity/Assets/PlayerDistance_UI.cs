﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDistance_UI : MonoBehaviour {

    public GameObject player;
    public GameObject target;
    Text value;

    float distance;

    void Start () {
        value = GetComponent<Text>();
	}
	
	void Update () {
        CalculateDistance();
        value.text = null;
        value.text = distance.ToString() + "u";
	}

    public void CalculateDistance()
    {
        distance = Vector3.Distance(target.transform.position, player.transform.position);
    }
}
