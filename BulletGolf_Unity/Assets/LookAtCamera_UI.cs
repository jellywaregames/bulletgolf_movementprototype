﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera_UI : MonoBehaviour {

    public GameObject cameraTarget;

    void Start()
    {
        cameraTarget = GameObject.Find("Main Camera");
    }

    void Update () {
        this.transform.LookAt(cameraTarget.transform.position);
    }
}
