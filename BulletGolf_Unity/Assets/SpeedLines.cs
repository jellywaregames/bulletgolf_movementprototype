﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedLines : MonoBehaviour {
    BulletMovementController bea;
    public GameObject beaGO;
    ParticleSystem ps;

    // Use this for initialization
    void Start () {
        bea = beaGO.GetComponent<BulletMovementController>();
        ps = GetComponent<ParticleSystem>();

    }

    // Update is called once per frame
    void Update() {

        var em = ps.emission;

        if (bea.tempSpeed > 3f)
        {

            em.enabled = true;
        }
        else
        {
            em.enabled = false;
        }
	}
}
