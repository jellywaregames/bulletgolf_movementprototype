﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialMeter : MonoBehaviour {

    public Transform followTarget;
    public Transform cameraTarget;

    public float currentValue = 5;
    public Vector2 maxMinValue;
    public Color[] colors;

    bool countDown = false;
    GameObject meterParent;

    Image meterFill;
    Image meterOn;
    Text meterValue;

    private void Awake()
    {
        meterParent = transform.GetChild(0).gameObject;
        meterFill = meterParent.transform.Find("RadialMeter_fill").GetComponent<Image>();
        meterOn = transform.Find("RadialMeter_on").GetComponent<Image>();
        meterValue = meterParent.transform.Find("RadialMeter_value").GetComponent<Text>();
        MeterEnabled(false);

        UpdateValue();
        UpdateFill();
    }

    private void Update()
    {
        if (countDown)
        {
            if (currentValue > maxMinValue.x)
            {
                currentValue -= 1f / 1 * Time.deltaTime;
            }
            else
            {
                countDown = false;
                meterParent.SetActive(false);
            }  
            UpdateValue();
            UpdateFill();
        }
        FollowTarget();
        /*
        if (Input.GetKeyDown(KeyCode.Space) && currentValue > maxMinValue.x)
        {

        }else if (Input.GetAxis("LoveTime") > 0 || Input.GetButtonDown("LoveTime"))
        {
            MeterEnabled(true);
        }
        else if (Input.GetAxis("LoveTime") < 1 || Input.GetButtonUp("LoveTime"))
        {
            MeterEnabled(false);
        }
        */
    }

    public void MeterEnabled(bool enabled)
    {
        if (enabled)
        {
            countDown = true;
            meterParent.SetActive(true);
            meterOn.color = colors[0];
        }
        else if (!enabled)
        {
            countDown = false;
            meterParent.SetActive(false);
            meterOn.color = colors[1];
        }
    }


    public void FollowTarget()
    {
        if (followTarget != null)
        {
            this.transform.position = followTarget.position;
            this.transform.LookAt(cameraTarget.position);
        }
    }

    public void ChangeVisible(bool visible)
    {
        if (visible)
        {


        }else if (!visible)
        {
            meterParent.transform.localScale -= new Vector3(5f,5f,0);
        }
    }

    public void UpdateValue()
    {
        //int value = Mathf.CeilToInt(currentValue);
        //meterValue.text = value.ToString();

        float value = currentValue;
        meterValue.text = value.ToString();
    }
    public void UpdateFill()
    {
        float value = currentValue;
        meterFill.fillAmount = (value /maxMinValue.y);
        meterOn.fillAmount = (value /maxMinValue.y);
    }









}
